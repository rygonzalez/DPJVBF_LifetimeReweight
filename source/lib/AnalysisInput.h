// Dear emacs, this is -*- c++ -*-
#ifndef FULLEXAMPLE_MYLIBRARY_H
#define FULLEXAMPLE_MYLIBRARY_H

// Local include(s):
#include "enums.h"

// ROOT includes(s):
#include "TMath.h"
#include "TTree.h"
#include "TFile.h"
#include "TSystem.h"
#include "TChain.h"

// Boost include(s):
// #include <boost/program_options.hpp>
//#include <boost/progress.hpp> // INCLUDE THIS ONE
//#include <boost/timer/progress_display.hpp>

// Text include(s):
#include <fstream>

// Vector include(s):
#include <vector>

//#include "TTreeReader.h"
//#include "TTreeReaderArray.h"

class AnalysisInput {

public:
  
  AnalysisInput(Option option);
  ~AnalysisInput();
  void addBranches();
  void initialize();
  std::vector<double> finalize(); // was double
  void execute(float newctau);
  float ctau(float partX, float partY, float partZ, float partE, float partM);
  float notZero(float numerator, float divisor);
  float newWeight(float t_gen, float t_new, float t_i); //lifetime reweighting
  
private:

  Option option_;
  TFile *output;
  TTree *tree_out;
  TFile *input;
  TChain *chain_in;
 
  // File name
  TString fname;
  TString outname;
  
  // Tree input variables

  // Number of events
  int n_events;
  int n_zero;
  
  // Run & trigger
  Int_t dsid;
  Bool_t metTrig;

  // Weight  
  Float_t m_weight;
  Float_t scale1fb;
  Float_t intLumi;
  Double_t metTrig_weight;

  // Jets
  Float_t jet1_pt;
  Float_t jet1_eta;
  Float_t jet1_phi;
  Float_t jet1_e;
  Float_t jet2_pt;
  Float_t jet2_eta;
  Float_t jet2_phi;
  Float_t jet2_e;
  Int_t njet30;
  Float_t mjj;
  Float_t detajj;
  Float_t signetajj;
  Float_t dphijj;
  Float_t absdphijj; // added for output tree
  Int_t hasBjet;

  // MET
  Float_t MET;
  Float_t METsig;
  Float_t METOSqrtHT;

  // MET - Jet
  Float_t dphi_j1met;
  Float_t min_dphi_jetmet;

  // Overall LJ
  Int_t nLJ20;

  // DPJ - hadronic
  //Int_t nLJjets;
  Int_t nLJjets20;
  Float_t LJjet1_pt;
  Float_t LJjet1_eta;
  Float_t LJjet1_phi;
  Float_t LJjet1_m;
  Float_t LJjet1_width;
  Float_t LJjet1_EMfrac;
  Float_t LJjet1_timing;
  Float_t LJjet1_jvt;
  Float_t LJjet1_gapRatio;
  Float_t LJjet1_BIBtagger;
  Float_t LJjet1_DPJtagger;
  Float_t LJjet1_isoID;
  Int_t LJ1_type;

  //Truth information
  Int_t LJjet1_truthDPidx;
  std::vector<Float_t> *truthPt = 0; // for LR test

  std::vector<Float_t> *truthPdgId = 0;
  std::vector<Float_t> *truthE = 0;
  std::vector<Float_t> *truthDecayVtx_x = 0;
  std::vector<Float_t> *truthDecayVtx_y = 0;
  std::vector<Float_t> *truthDecayVtx_z = 0;
  std::vector<Float_t> *truthDecayType = 0;
  float ct1;
  float ct2;
  float w1;
  float w2;

  // DPJ - muonic
  Int_t nLJmus20;
  Int_t neleSignal;
  Int_t nmuSignal;
  Int_t neleBaseline;
  Int_t nmuBaseline;

  // Tree files folder
  TString filesFolder = "/home/richards/WorkArea/DPJanalysis/v02-00/frvz_vbf_wMETtrigWeights/"; // final metTrig SFs
 
  // Pico to femto
  int picoFemto = 1000;
  // Luminosity @ LHC Run-2 (fb-1)
  int lum = 140;

  // Dark photon masses & ctaus
  float m_dpj_500757 = 0.1; //GeV
  float m_dpj_500758 = 0.4;
  float m_dpj_500759 = 0.4;
  float m_dpj_500760 = 0.4;
  float m_dpj_500761 = 10;
  float m_dpj_500762 = 15;
  float m_dpj_500763 = 10;
  float m_dpj_500764 = 15;

  float m_dpj_521257 = 0.017;
  float m_dpj_521258 = 0.05;
  float m_dpj_521259 = 0.9;
  float m_dpj_521260 = 2;
  float m_dpj_521261 = 6;
  float m_dpj_521262 = 25;
  float m_dpj_521263 = 40;

  float ctau_gen_500757 = 15; //mm
  float ctau_gen_500758 = 50;
  float ctau_gen_500759 = 5;
  float ctau_gen_500760 = 500;
  float ctau_gen_500761 = 900;
  float ctau_gen_500762 = 1000;
  float ctau_gen_500763 = 900;
  float ctau_gen_500764 = 1000;

  float ctau_gen_521257 = 2;
  float ctau_gen_521258 = 7;
  float ctau_gen_521259 = 115;
  float ctau_gen_521260 = 175;
  float ctau_gen_521261 = 600;
  float ctau_gen_521262 = 1200;
  float ctau_gen_521263 = 1400;

  // Counter events
  int count[14] = { 0 };
  int n_counters = 14; //number of cuts
  float total_weight[14] = { 0 };
  float sum_w2[14] = { 0 };

  // csv files
  //std::ofstream MCEv_VBF;
  //std::ofstream Ev_VBF;

};

#endif
