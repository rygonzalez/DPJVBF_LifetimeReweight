// System include(s):
#include <iostream>
//#include <iomanip>

// Local include(s):
#include "AnalysisInput.h"

// ROOT include(s):
#include "TTree.h"

std::vector<double> Run( Option opt, float new_ctau ) {

  AnalysisInput *DF = new AnalysisInput(opt);
  DF->initialize();
  DF->addBranches();
  DF->execute(new_ctau);
  std::vector<double> effxacc_n_error = DF->finalize();
  delete DF;

  // Return gracefully:
  return effxacc_n_error;
}

int main( int argc, char* argv[] ) {

  std::ofstream eff_vs_ctaus;
  eff_vs_ctaus.open("../eff_vs_ctaus.csv");
  eff_vs_ctaus << "ctau" << "," << "efficiency" << "," << "eff_up" << "," << "eff_down"<< std::endl;

  // create vector of new ctaus
  //std::vector<float> newctau_list = {0.1,0.12,0.14,0.16,0.18,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.2,1.4,1.6,1.8,2,3,4,5,6,7,8,9,10,11,12,13,14};
  //for ( unsigned long int i = 15; i < 10005; i+=5 ) { // was i=1, i < 10001, ++i for steps of one
  //  newctau_list.push_back(i);
  //}

  //int ctau_nbins = 1000;
  //double ctau_min = 1e-1;
  //double ctau_max = 1e6;
  //double exp_ctau_min = TMath::Log10(ctau_min);
  //double exp_ctau_max = TMath::Log10(ctau_max);
  //double exp_ctau_step = (exp_ctau_max - exp_ctau_min) / (ctau_nbins);
  //std::vector<double> newctau_list;
  //for(int i=0; i<=ctau_nbins; i++){
  //  newctau_list.push_back( pow(10., exp_ctau_min + i*exp_ctau_step ) );
  //}

  // to use same ctau values than ggF/WH
  std::ifstream ctau_list_wh;
  ctau_list_wh.open("../source/app/ctau_list_wh.txt");
  double ctau_i;
  std::vector<double> newctau_list;
  for (int nline=1; nline<=1000; nline++) {
    ctau_list_wh >> ctau_i;
    newctau_list.push_back(ctau_i);
  }
  ctau_list_wh.close();

  // run and write to file
  for ( unsigned long int index = 0; index < newctau_list.size(); ++index ) {

    std::cout << std::endl;
    std::cout << "###############################################################" << std::endl;
    std::cout << std::endl;
    std::cout << "Extrapolating efficiency to ctau = " << newctau_list[index] << " mm" << std::endl;
    std::vector<double> effnerr = Run(sgn_VBF,newctau_list[index]);
    eff_vs_ctaus << newctau_list[index] << "," << effnerr[0] << "," << effnerr[0]+effnerr[1] << "," << effnerr[0]-effnerr[1] << std::endl;

  }

  //Run(sgn_VBF); // using before
  
  // Return gracefully:
  return 0;
  
}
