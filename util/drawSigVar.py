import numpy as np
import matplotlib.pyplot as plt
import csv
from enum import Enum
import sys
import logging

import matplotlib as mpl

import ROOT as root
from rootpy.plotting import Hist, HistStack, Legend, Canvas
from rootpy.plotting.style import get_style, set_style
from rootpy.plotting.utils import draw
import rootpy.plotting.root2matplotlib as rplt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

# to read csv - test
import pandas as pd
from collections import defaultdict
columns = defaultdict(list)

mpl_logger = logging.getLogger("matplotlib")
mpl_logger.setLevel(logging.WARNING)

# set the style
style = get_style('ATLAS')
style.SetEndErrorSize(3)
set_style(style)

df_DPJTagger = pd.read_csv ('Output/LJjet1_DPJtagger.csv')
Score_DPJTagger = df_DPJTagger.xmin
Sig_DPJTagger = df_DPJTagger.Sig_P

# ATLAS style
set_style('ATLAS', mpl=True)

fig, ax1 = plt.subplots()

ax1.xaxis.set_minor_locator(AutoMinorLocator())
ax1.yaxis.set_minor_locator(AutoMinorLocator())

color = 'tab:red'
ax1.set_xlabel('Mass [GeV]')
ax1.tick_params(axis='y')
ax1.set_ylabel('Significance')
ax1.plot(Score_DPJTagger, Sig_DPJTagger, linestyle=':', linewidth=2, color='red', label='Sig')

ax1.xaxis.set_label_coords(1., -0.12)
ax1.yaxis.set_label_coords(-0.12, 0.85)

#ax1.set_ylim([0,0.2])
#ax1.legend(loc='upper left', prop=dict(size=16))

mpl.rcParams['font.size'] = 16
mpl.rcParams['text.latex.preamble'] = [r'\usepackage{helvet}',
                                            r'\usepackage{sansmath}',
                                            r'\setlength{\parindent}{0pt}'
                                            r'\sansmath']

ax1.xaxis.set_minor_locator(AutoMinorLocator())
ax1.yaxis.set_minor_locator(AutoMinorLocator())

#if setLog == True: plt.yscale('log')

plt.rc('text', usetex = True)
tex_label = (r'\textbf{\textit{ATLAS}} Work in progress')
tex_process = (r'H $\rightarrow$ ZZ* $\rightarrow$ 4l')
tex_lum = (r'$\sqrt{s}$ = 13 TeV, L = 140 $fb^{-1}$')
tex_SR = (r'SR: 200 GeV $< m_{4l} <$ 2000 GeV')

ymax = 0.92
xmax = 0.75
ax1.text(xmax, ymax, tex_label,
         verticalalignment='center', horizontalalignment='center',
          transform=ax1.transAxes, fontsize=20)
ax1.text(xmax-0.13, ymax-0.08, tex_process,
          verticalalignment='center', horizontalalignment='center',
          transform=ax1.transAxes, fontsize=15)
ax1.text(xmax-0.09, ymax-0.14, tex_lum,
          verticalalignment='center', horizontalalignment='center',
          transform=ax1.transAxes, fontsize=12)
ax1.text(xmax-0.09, ymax-0.2, tex_SR,
          verticalalignment='center', horizontalalignment='center',
          transform=ax1.transAxes, fontsize=9)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.savefig('plots/Significance/SigAll.png')
plt.close()
