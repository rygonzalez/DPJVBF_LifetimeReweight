import ROOT 
ROOT.gROOT.SetBatch(True)

import os
cwd = os.getcwd()
stylepath = cwd + '/AtlasStyle/'
print(stylepath)

ROOT.gROOT.LoadMacro( stylepath + "AtlasStyle.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasLabels.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasUtils.C")
ROOT.SetAtlasStyle()

from ROOT import ATLASLabel
import math
import numpy as np
import csv
from enum import Enum
import sys

#Flags
doLogX = False
doLogY = False
Normalise = True
allBkg = False

#basepath = '/home/saolivap/WorkArea/samples/DarkPhoton/ntuples'
basepath = '/home/rygonzalez/WorkArea/ATLAS_samples/DarkPhoton/v01-06'

#tree = 'Selection'
tree = 'miniT'
hists = ['jet1_pt', 'jet1_eta', 'jet1_phi', 'jet1_e'
         ,'jet2_pt', 'jet2_eta', 'jet2_phi', 'jet2_e'
         ,'mjj', 'detajj', 'signetajj', 'dphijj', 'dphi_j1met', 'min_dphi_jetmet'
         ,'nLJjets20', 'LJjet1_pt', 'LJjet1_eta', 'LJjet1_phi', 'LJjet1_m'
         ,'LJjet1_width', 'LJjet1_EMfrac', 'LJjet1_timing', 'LJjet1_jvt', 'LJjet1_gapRatio'
##         ,'LJjet1_IsBIB', 'LJjet1_DPJtagger', 'LJjet1_truthDPidx',
         ,'MET', 'METOSqrtHT', 'METsig'
         ,'LJjet1_DPJtagger'
         ,'HT_20', 'LJjet_EMfrac', 'LJjet_width'
         ,'nLJ20'
         ]
stacks = ['hs']
merges = ['AllBkg']

samples = {
    'sgn_VBF_500758' : [basepath + '/frvz_vbf_500758.root'],
    'bkg_QCD'        : [basepath + '/qcd_main.root'],    
    'bkg_top'        : [basepath + '/top_powheg.root'],    
    'bkg_VV'         : [basepath + '/diboson_sherpa221.root'],    
    'bkg_Wjets'      : [basepath + '/wjets_sherpa221.root'],    
    'bkg_Zjets'      : [basepath + '/zjets_sherpa221.root'],    
    'bkg_Znn'        : [basepath + '/znnjets_sherpa221.root'],    
}

# CREATING HISTOGRAMS
h = {}
for sample in samples.keys():
    h[sample] = {}

    for hist in hists:
        h[sample][hist] = {}

        n = 'h_%s_%s' % (sample, hist)

        if hist == 'jet1_pt' or hist == 'jet2_pt' or hist == 'LJjet1_pt': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 500000)
        elif hist == 'jet1_eta' or hist == 'jet2_eta' or hist == 'LJjet1_eta': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, -3, 3)
        elif hist == 'jet1_phi' or hist == 'jet2_phi' or hist == 'dphijj' or hist == 'LJjet1_phi': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, -3.4, 3.4)
        elif hist == 'dphi_j1met' or hist == 'min_dphi_jetmet': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 3.4)
        elif hist == 'LJjet1_m': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 300000)
        elif hist == 'jet1_e' or hist == 'jet2_e': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 700000)
        elif hist == 'mjj': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 100000, 6000000)
        elif hist == 'detajj': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 3, 9)
        elif hist == 'signetajj': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, -1.1, 1.1)
        elif hist == 'nLJjets20' or hist == 'nLJ20': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 6, 0, 5)
        elif hist == 'LJjet1_EMfrac' or hist == 'LJjet_EMfrac': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 1)
        elif hist == 'LJjet1_gapRatio': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 1)
        elif hist == 'LJjet1_IsBIB': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 0.00001)
        elif hist == 'LJjet1_jvt': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 1)
        elif hist == 'LJjet1_width' or hist == 'LJjet_width': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 0.3)
        elif hist == 'LJjet1_timing': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, -10, 10)
        elif hist == 'LJjet1_DPJtagger': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 1)
        elif hist == 'LJjet1_truthDPidx': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 10)
        elif hist == 'MET': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 2000000)
        elif hist == 'METsig': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 80)
        elif hist == 'METOSqrtHT': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 25)
        elif hist == 'HT_20': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 1400000)


# CREATING MERGE HISTOGRAMS
g = {}
for merge in merges:
    g[merge] = {}

    for hist in hists:
        g[merge][hist] = {}

        n = 'h_%s_%s' % (merge, hist)

        if hist == 'jet1_pt' or hist == 'jet2_pt' or hist == 'LJjet1_pt': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 500000)
        elif hist == 'jet1_eta' or hist == 'jet2_eta' or hist == 'LJjet1_eta': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, -3, 3)
        elif hist == 'jet1_phi' or hist == 'jet2_phi' or hist == 'dphijj' or hist == 'LJjet1_phi': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, -3.4, 3.4)
        elif hist == 'dphi_j1met' or hist == 'min_dphi_jetmet': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 3.4)
        elif hist == 'LJjet1_m': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 300000)
        elif hist == 'jet1_e' or hist == 'jet2_e': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 700000)
        elif hist == 'mjj': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 100000, 6000000)
        elif hist == 'detajj': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 3, 9)
        elif hist == 'signetajj': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, -1.1, 1.1)
        elif hist == 'nLJjets20' or hist == 'nLJ20': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 6, 0, 5)
        elif hist == 'LJjet1_EMfrac' or hist == 'LJjet_EMfrac': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 1)
        elif hist == 'LJjet1_gapRatio': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 1)
        elif hist == 'LJjet1_IsBIB': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 0.00001)
        elif hist == 'LJjet1_jvt': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 1)
        elif hist == 'LJjet1_width' or hist == 'LJjet_width': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 0.3)
        elif hist == 'LJjet1_timing': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, -10, 10)
        elif hist == 'LJjet1_DPJtagger': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 1)
        elif hist == 'LJjet1_truthDPidx': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 10)
        elif hist == 'MET': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 2000000)
        elif hist == 'METsig': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 80)
        elif hist == 'METOSqrtHT': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 25)
        elif hist == 'HT_20': g[merge][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, merge), 50, 0, 1400000)

        
# FILLING
for hist in hists:
    print( 'processing histogram: %s' % hist)

    for sample in samples.keys():
        print ('processing sample: %s' % sample)
        l = samples[sample]
        
        for fname in l:
            print ('            > %s' % fname)
            f = ROOT.TFile.Open(fname)
            t = f.Get(tree)
            
            if hist == 'jet1_pt' or hist == 'jet2_pt' or hist == 'LJjet1_pt': n = 'htmp(50,0,500000)'
            elif hist == 'jet1_eta' or hist == 'jet2_eta' or hist == 'LJjet1_eta': n = 'htmp(50,-3,3)'
            elif hist == 'dphi_j1met' or hist == 'min_dphi_jetmet': n = 'htmp(50,0,3.4)'
            elif hist == 'jet1_phi' or hist == 'jet2_phi' or hist == 'dphijj' or hist == 'LJjet1_phi': n = 'htmp(50,-3.4,3.4)'
            elif hist == 'jet1_e' or hist == 'jet2_e': n = 'htmp(50,0,700000)'
            elif hist == 'LJjet1_m': n = 'htmp(50,0,300000)'
            elif hist == 'mjj': n = 'htmp(50,100000,6000000)'
            elif hist == 'detajj': n = 'htmp(50,3,9)'
            elif hist == 'signetajj': n = 'htmp(50,-1.1,1.1)'
            elif hist == 'nLJjets20' or hist == 'nLJ20': n = 'htmp(6,0,5)'
            elif hist == 'LJjet1_width' or hist == 'LJjet_width': n = 'htmp(50,0,0.3)'
            elif hist == 'LJjet1_EMfrac' or hist == 'LJjet_EMfrac': n = 'htmp(50,0,1)'
            elif hist == 'LJjet1_jvt': n = 'htmp(50,0,1)'
            elif hist == 'LJjet1_gapRatio': n = 'htmp(50,0,1)'
            elif hist == 'LJjet1_IsBIB': n = 'htmp(50,0,0.00001)'
            elif hist == 'LJjet1_timing': n = 'htmp(50,-10,10)'
            elif hist == 'LJjet1_DPJtagger': n = 'htmp(50,0,1)'
            elif hist == 'LJjet1_truthDPidx': n = 'htmp(50,0,10)'
            elif hist == 'MET': n = 'htmp(50,0,2000000)'
            elif hist == 'METsig': n = 'htmp(50,0,80)'
            elif hist == 'METOSqrtHT': n = 'htmp(50,0,25)'
            elif hist == 'HT_20': n = 'htmp(50,0,1400000)'

            if t.Draw('%s>>%s' % (hist, n), '1*scale1fb*intLumi', 'goff'): h[sample][hist].Add(ROOT.gDirectory.Get('htmp'), 1.)                
del f

# MERGING SAMPLES
for merge in merges:
    for hist in hists:
        
        if merge == 'AllBkg':
            g[merge][hist].Add(h['bkg_QCD'][hist], 1)
            g[merge][hist].Add(h['bkg_top'][hist], 1)
            g[merge][hist].Add(h['bkg_VV'][hist], 1)
            g[merge][hist].Add(h['bkg_Wjets'][hist], 1)
            g[merge][hist].Add(h['bkg_Zjets'][hist], 1)
            g[merge][hist].Add(h['bkg_Znn'][hist], 1)

# YIELDS
for hist in hists:
    print( 'processing histogram: %s' % hist)

    if hist == 'jet1_pt' or hist == 'jet2_pt':
        xMin = 30000 
        xMax = 500000
        minSel = np.empty(45) # number of points
        rangeSel = np.arange(xMin, xMax, 10000) # steps between xMin and xMax
    elif hist == 'jet1_eta' or hist == 'jet2_eta':
        xMin = -3 
        xMax = 3
        minSel = np.empty(120) # number of points
        rangeSel = np.arange(xMin, xMax, 0.05) # steps between xMin and xMax
    elif hist == 'jet1_phi' or hist == 'jet2_phi':
        xMin = -3.4 
        xMax = 3.4
        minSel = np.empty(120) # number of points
        rangeSel = np.arange(xMin, xMax, 0.05) # steps between xMin and xMax
    elif hist == 'jet1_e' or hist == 'jet2_e':
        xMin = 90000 
        xMax = 700000
        minSel = np.empty(60) # number of points
        rangeSel = np.arange(xMin, xMax, 10000) # steps between xMin and xMax
    elif hist == 'LJjet1_DPJtagger':
        xMin = 0 
        xMax = 1
        minSel = np.empty(100) # number of points
        rangeSel = np.arange(xMin, xMax, 0.01) # steps between xMin and xMax
    elif hist == 'MET':
        xMin = 90000 
        xMax = 2000000
        minSel = np.empty(120) # number of points
        rangeSel = np.arange(xMin, xMax, 10000) # steps between xMin and xMax
    elif hist == 'METsig':
        xMin = 0 
        xMax = 50
        minSel = np.empty(100) # number of points
        rangeSel = np.arange(xMin, xMax, 0.5) # steps between xMin and xMax
    elif hist == 'METOSqrtHT':
        xMin = 0 
        xMax = 25
        minSel = np.empty(50) # number of points
        rangeSel = np.arange(xMin, xMax, 0.5) # steps between xMin and xMax
    elif hist == 'mjj':
        xMin = 80000 
        xMax = 5000000
        minSel = np.empty(120) # number of points
        rangeSel = np.arange(xMin, xMax, 10000) # steps between xMin and xMax
    elif hist == 'detajj':
        xMin = 3
        xMax = 9
        minSel = np.empty(120) # number of points
        rangeSel = np.arange(xMin, xMax, 0.05) # steps between xMin and xMax
    elif hist == 'signetajj':
        xMin = -1
        xMax = 1
        minSel = np.empty(41) # number of points
        rangeSel = np.arange(xMin, xMax, 0.05) # steps between xMin and xMax
    elif hist == 'dphijj':
        xMin = -3.4
        xMax = 3.4
        minSel = np.empty(137) # number of points
        rangeSel = np.arange(xMin, xMax, 0.05) # steps between xMin and xMax
    elif hist == 'dphi_j1met':
        xMin = 0
        xMax = 3.4
        minSel = np.empty(60) # number of points
        rangeSel = np.arange(xMin, xMax, 0.05) # steps between xMin and xMax
    elif hist == 'min_dphi_jetmet':
        xMin = 0
        xMax = 3.4
        minSel = np.empty(60) # number of points
        rangeSel = np.arange(xMin, xMax, 0.05) # steps between xMin and xMax
    elif hist == 'nLJ20' or hist == 'nLJjets20':
        xMin = 0
        xMax = 5
        minSel = np.empty(5) # number of points
        rangeSel = np.arange(xMin, xMax, 1) # steps between xMin and xMax
    elif hist == 'HT_20':
        xMin = 0
        xMax = 1400000
        minSel = np.empty(120)
        rangeSel = np.arange(xMin, xMax, 10000)
    elif hist == 'LJjet1_pt' :
        xMin = 30000 
        xMax = 500000
        minSel = np.empty(45) # number of points
        rangeSel = np.arange(xMin, xMax, 10000) # steps between xMin and xMax
    elif hist == 'LJjet1_eta' :
        xMin = -3 
        xMax = 3
        minSel = np.empty(120) # number of points
        rangeSel = np.arange(xMin, xMax, 0.05) # steps between xMin and xMax
    elif hist == 'LJjet1_phi' :
        xMin = -3.4 
        xMax = 3.4
        minSel = np.empty(120) # number of points
        rangeSel = np.arange(xMin, xMax, 0.05) # steps between xMin and xMax
    elif hist == 'LJjet1_m' :
        xMin = 90000 
        xMax = 700000
        minSel = np.empty(60) # number of points
        rangeSel = np.arange(xMin, xMax, 10000) # steps between xMin and xMax
    elif hist == 'LJjet1_EMfrac' or hist == 'LJjet_EMfrac':
        xMin = 0
        xMax = 1
        minSel = np.empty(100)
        rangeSel = np.arange(xMin, xMax, 0.01)
    elif hist == 'LJjet_width' or hist == 'LJjet1_width':
        xMin = 0
        xMax = 0.3
        minSel = np.empty(60)
        rangeSel = np.arange(xMin, xMax, 0.005)
    elif hist == 'LJjet1_timing' :
        xMin = -10 
        xMax = 10
        minSel = np.empty(41) # number of points
        rangeSel = np.arange(xMin, xMax, 0.5) # steps between xMin and xMax
    elif hist == 'LJjet1_jvt' :
        xMin = 0 
        xMax = 1
        minSel = np.empty(100) # number of points
        rangeSel = np.arange(xMin, xMax, 0.01) # steps between xMin and xMax
    elif hist == 'LJjet1_gapRatio' :
        xMin = 0
        xMax = 1
        minSel = np.empty(100) # number of points
        rangeSel = np.arange(xMin, xMax, 0.01) # steps between xMin and xMax
        
    ind = np.arange(len(minSel))
    np.put(minSel, ind, rangeSel)

    if hist == 'jet1_pt': f = open('Output/jet1_pt.csv', 'w+')
    elif hist == 'jet1_eta': f = open('Output/jet1_eta.csv', 'w+')
    elif hist == 'jet1_phi': f = open('Output/jet1_phi.csv', 'w+')
    elif hist == 'jet1_e': f = open('Output/jet1_e.csv', 'w+')
    elif hist == 'jet2_pt': f = open('Output/jet2_pt.csv', 'w+')
    elif hist == 'jet2_eta': f = open('Output/jet2_eta.csv', 'w+')
    elif hist == 'jet2_phi': f = open('Output/jet2_phi.csv', 'w+')
    elif hist == 'jet2_e': f = open('Output/jet2_e.csv', 'w+')
    elif hist == 'LJjet1_DPJtagger': f = open('Output/LJjet1_DPJtagger.csv', 'w+')
    elif hist == 'MET': f = open('Output/MET.csv', 'w+')
    elif hist == 'METsig': f = open('Output/METsig.csv', 'w+')
    elif hist == 'METOSqrtHT': f = open('Output/METOSqrtHT.csv', 'w+')
    elif hist == 'mjj': f = open('Output/mjj.csv', 'w+')
    elif hist == 'detajj': f = open('Output/detajj.csv', 'w+')
    elif hist == 'signetajj': f = open('Output/signetajj.csv', 'w+')
    elif hist == 'dphijj': f = open('Output/dphijj.csv', 'w+')
    elif hist == 'dphi_j1met': f = open('Output/dphi_j1met.csv', 'w+')
    elif hist == 'min_dphi_jetmet': f = open('Output/min_dphi_jetmet.csv', 'w+')
    elif hist == 'nLJ20': f = open('Output/nLJ20.csv', 'w+')
    elif hist == 'nLJjets20': f = open('Output/nLJjets20.csv', 'w+')
    elif hist == 'HT_20': f = open('Output/HT_20.csv', 'w+')
    elif hist == 'LJjet1_pt': f = open('Output/LJjet1_pt.csv', 'w+')
    elif hist == 'LJjet1_eta': f = open('Output/LJjet1_eta.csv', 'w+')
    elif hist == 'LJjet1_phi': f = open('Output/LJjet1_phi.csv', 'w+')
    elif hist == 'LJjet1_m': f = open('Output/LJjet1_m.csv', 'w+')
    elif hist == 'LJjet_EMfrac': f = open('Output/LJjet_EMfrac.csv', 'w+')
    elif hist == 'LJjet1_EMfrac': f = open('Output/LJjet1_EMfrac.csv', 'w+')
    elif hist == 'LJjet_width': f = open('Output/LJjet_width.csv', 'w+')
    elif hist == 'LJjet1_width': f = open('Output/LJjet1_width.csv', 'w+')
    elif hist == 'LJjet1_timing': f = open('Output/LJjet1_timing.csv', 'w+')
    elif hist == 'LJjet1_jvt': f = open('Output/LJjet1_jvt.csv', 'w+')
    elif hist == 'LJjet1_gapRatio': f = open('Output/LJjet1_gapRatio.csv', 'w+')
    
    with f:
        writer = csv.writer(f)
        writer.writerow(['Var','Nbkg','Nsg','Sig_P','Sig_G'])

        for xmin in minSel:
    
            if hist == 'jet1_pt': axis = h['bkg_QCD']['jet1_pt'].GetXaxis()
            if hist == 'jet1_eta': axis = h['bkg_QCD']['jet1_eta'].GetXaxis()
            if hist == 'jet1_phi': axis = h['bkg_QCD']['jet1_phi'].GetXaxis()
            if hist == 'jet1_e': axis = h['bkg_QCD']['jet1_e'].GetXaxis()
            if hist == 'jet2_pt': axis = h['bkg_QCD']['jet2_pt'].GetXaxis()
            if hist == 'jet2_eta': axis = h['bkg_QCD']['jet2_eta'].GetXaxis()
            if hist == 'jet2_phi': axis = h['bkg_QCD']['jet2_phi'].GetXaxis()
            if hist == 'jet2_e': axis = h['bkg_QCD']['jet2_e'].GetXaxis()
            if hist == 'LJjet1_DPJtagger': axis = h['bkg_QCD']['LJjet1_DPJtagger'].GetXaxis()
            if hist == 'MET': axis = h['bkg_QCD']['MET'].GetXaxis()
            if hist == 'METsig': axis = h['bkg_QCD']['METsig'].GetXaxis()
            if hist == 'METOSqrtHT': axis = h['bkg_QCD']['METOSqrtHT'].GetXaxis()
            if hist == 'mjj': axis = h['bkg_QCD']['mjj'].GetXaxis()
            if hist == 'detajj': axis = h['bkg_QCD']['detajj'].GetXaxis()
            if hist == 'signetajj': axis = h['bkg_QCD']['signetajj'].GetXaxis()
            if hist == 'dphijj': axis = h['bkg_QCD']['dphijj'].GetXaxis()
            if hist == 'dphi_j1met': axis = h['bkg_QCD']['dphi_j1met'].GetXaxis()
            if hist == 'min_dphi_jetmet': axis = h['bkg_QCD']['min_dphi_jetmet'].GetXaxis()
            if hist == 'nLJ20': axis = h['bkg_QCD']['nLJ20'].GetXaxis()
            if hist == 'nLJjets20': axis = h['bkg_QCD']['nLJjets20'].GetXaxis()
            if hist == 'HT_20': axis = h['bkg_QCD']['HT_20'].GetXaxis()
            if hist == 'LJjet1_pt': axis = h['bkg_QCD']['LJjet1_pt'].GetXaxis()
            if hist == 'LJjet1_eta': axis = h['bkg_QCD']['LJjet1_eta'].GetXaxis()
            if hist == 'LJjet1_phi': axis = h['bkg_QCD']['LJjet1_phi'].GetXaxis()
            if hist == 'LJjet1_m': axis = h['bkg_QCD']['LJjet1_m'].GetXaxis()
            if hist == 'LJjet_EMfrac': axis = h['bkg_QCD']['LJjet_EMfrac'].GetXaxis()
            if hist == 'LJjet1_EMfrac': axis = h['bkg_QCD']['LJjet1_EMfrac'].GetXaxis()
            if hist == 'LJjet_width': axis = h['bkg_QCD']['LJjet_width'].GetXaxis()
            if hist == 'LJjet1_width': axis = h['bkg_QCD']['LJjet1_width'].GetXaxis()
            if hist == 'LJjet1_timing': axis = h['bkg_QCD']['LJjet1_timing'].GetXaxis()
            if hist == 'LJjet1_jvt': axis = h['bkg_QCD']['LJjet1_jvt'].GetXaxis()
            if hist == 'LJjet1_gapRatio': axis = h['bkg_QCD']['LJjet1_gapRatio'].GetXaxis()

            bmin = axis.FindBin(xmin)
            bmax = axis.FindBin(xMax)
            if hist == 'jet1_pt':
                Nbkg = g['AllBkg']['jet1_pt'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['jet1_pt'].Integral(bmin,bmax)
            if hist == 'jet1_eta':
                Nbkg = g['AllBkg']['jet1_eta'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['jet1_eta'].Integral(bmin,bmax)
            if hist == 'jet1_phi':
                Nbkg = g['AllBkg']['jet1_phi'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['jet1_phi'].Integral(bmin,bmax)
            if hist == 'jet1_e':
                Nbkg = g['AllBkg']['jet1_e'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['jet1_e'].Integral(bmin,bmax)
            if hist == 'jet2_pt':
                Nbkg = g['AllBkg']['jet2_pt'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['jet2_pt'].Integral(bmin,bmax)
            if hist == 'jet2_eta':
                Nbkg = g['AllBkg']['jet2_eta'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['jet2_eta'].Integral(bmin,bmax)
            if hist == 'jet2_phi':
                Nbkg = g['AllBkg']['jet2_phi'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['jet2_phi'].Integral(bmin,bmax)
            if hist == 'jet2_e':
                Nbkg = g['AllBkg']['jet2_e'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['jet2_e'].Integral(bmin,bmax)
            if hist == 'LJjet1_DPJtagger':
                Nbkg = g['AllBkg']['LJjet1_DPJtagger'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_DPJtagger'].Integral(bmin,bmax)
            if hist == 'MET':
                Nbkg = g['AllBkg']['MET'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['MET'].Integral(bmin,bmax)
            if hist == 'METsig':
                Nbkg = g['AllBkg']['METsig'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['METsig'].Integral(bmin,bmax)
            if hist == 'METOSqrtHT':
                Nbkg = g['AllBkg']['METOSqrtHT'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['METOSqrtHT'].Integral(bmin,bmax)
            if hist == 'mjj':
                Nbkg = g['AllBkg']['mjj'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['mjj'].Integral(bmin,bmax)
            if hist == 'detajj':
                Nbkg = g['AllBkg']['detajj'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['detajj'].Integral(bmin,bmax)
            if hist == 'signetajj':
                Nbkg = g['AllBkg']['signetajj'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['signetajj'].Integral(bmin,bmax)
            if hist == 'dphijj':
                Nbkg = g['AllBkg']['dphijj'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['dphijj'].Integral(bmin,bmax)
            if hist == 'dphi_j1met':
                Nbkg = g['AllBkg']['dphi_j1met'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['dphi_j1met'].Integral(bmin,bmax)
            if hist == 'min_dphi_jetmet':
                Nbkg = g['AllBkg']['min_dphi_jetmet'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['min_dphi_jetmet'].Integral(bmin,bmax)
            if hist == 'nLJ20':
                Nbkg = g['AllBkg']['nLJ20'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['nLJ20'].Integral(bmin,bmax)
            if hist == 'nLJjets20':
                Nbkg = g['AllBkg']['nLJjets20'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['nLJjets20'].Integral(bmin,bmax)
            if hist == 'HT_20':
                Nbkg = g['AllBkg']['HT_20'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['HT_20'].Integral(bmin,bmax)
            if hist == 'LJjet1_pt':
                Nbkg = g['AllBkg']['LJjet1_pt'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_pt'].Integral(bmin,bmax)
            if hist == 'LJjet1_eta':
                Nbkg = g['AllBkg']['LJjet1_eta'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_eta'].Integral(bmin,bmax)
            if hist == 'LJjet1_phi':
                Nbkg = g['AllBkg']['LJjet1_phi'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_phi'].Integral(bmin,bmax)
            if hist == 'LJjet1_m':
                Nbkg = g['AllBkg']['LJjet1_m'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_m'].Integral(bmin,bmax)
            if hist == 'LJjet_EMfrac':
                Nbkg = g['AllBkg']['LJjet_EMfrac'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet_EMfrac'].Integral(bmin,bmax)
            if hist == 'LJjet1_EMfrac':
                Nbkg = g['AllBkg']['LJjet1_EMfrac'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_EMfrac'].Integral(bmin,bmax)
            if hist == 'LJjet_width':
                Nbkg = g['AllBkg']['LJjet_width'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet_width'].Integral(bmin,bmax)
            if hist == 'LJjet1_width':
                Nbkg = g['AllBkg']['LJjet1_width'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_width'].Integral(bmin,bmax)
            if hist == 'LJjet1_timing':
                Nbkg = g['AllBkg']['LJjet1_timing'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_timing'].Integral(bmin,bmax)
            if hist == 'LJjet1_jvt':
                Nbkg = g['AllBkg']['LJjet1_jvt'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_jvt'].Integral(bmin,bmax)
            if hist == 'LJjet1_gapRatio':
                Nbkg = g['AllBkg']['LJjet1_gapRatio'].Integral(bmin,bmax)
                Nsgn = h['sgn_VBF_500758']['LJjet1_gapRatio'].Integral(bmin,bmax)

            if Nbkg > 0:
                Sig_P = math.sqrt( 2*( (Nsgn+Nbkg)*math.log(1+Nsgn/Nbkg)-Nsgn ) )
                Sig_G = Nsgn / math.sqrt(Nbkg)
            else:
                Sig_P = 0
                Sig_G = 0
                
            print ('Var: %.3f; Total bkg events: %.4f; Total sgn events: %.5f; Significance_P: %.4f; Significance_G: %.4f' % (xmin, Nbkg, Nsgn, Sig_P, Sig_G))
            #print ('QCD bkg: %.2f; Total bkg: %.2f' % (h['bkg_QCD']['jet1_pt'].Integral(bmin,bmax), g['AllBkg']['jet1_pt'].Integral(bmin,bmax)))
            writer.writerow( ['%.3f' % xmin, '%.4f' % Nbkg, '%.5f'% Nsgn, '%.4f'% Sig_P, '%.5f'% Sig_G] )

    f.close()

