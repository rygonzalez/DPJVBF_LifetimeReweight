import ROOT 
ROOT.gROOT.SetBatch(True)

import os
cwd = os.getcwd()
stylepath = cwd + '/AtlasStyle/'
print(stylepath)

ROOT.gROOT.LoadMacro( stylepath + "AtlasStyle.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasLabels.C")
ROOT.gROOT.LoadMacro( stylepath + "AtlasUtils.C")
ROOT.SetAtlasStyle()

from ROOT import ATLASLabel
#import math
#import numpy as np
#import csv
#from enum import Enum
#import sys

#Flags
doLogX = False
doLogY = False
Normalise = True
allBkg = False

basepath = '/home/saolivap/WorkArea/samples/DarkPhoton/ntuples'

tree = 'Selection'
hists = ['jet1_pt', 'jet1_eta', 'jet1_phi', 'jet1_e'
         ,'jet2_pt', 'jet2_eta', 'jet2_phi', 'jet2_e'
         ,'mjj', 'detajj', 'signetajj', 'dphijj', 'dphi_j1met', 'min_dphi_jetmet'
         ,'nLJjets20', 'LJjet1_pt', 'LJjet1_eta', 'LJjet1_phi', 'LJjet1_m'
         ,'LJjet1_width', 'LJjet1_EMfrac', 'LJjet1_timing', 'LJjet1_jvt', 'LJjet1_gapRatio'
         ,'LJjet1_IsBIB', 'LJjet1_DPJtagger', 'LJjet1_truthDPidx'
         ,'MET', 'METOSqrtHT', 'METsig'
         ,'njet30'
         ]
stacks = ['hs']

samples = {
    'sgn_VBF_500758' : [basepath + '/VBF_sgn_ntuple.root'],
    'bkg_QCD'        : [basepath + '/QCD_bkg_ntuple.root'],    
    'bkg_top'        : [basepath + '/Top_bkg_ntuple.root'],    
    'bkg_VV'         : [basepath + '/VV_bkg_ntuple.root'],    
    'bkg_Wjets'      : [basepath + '/Wjets_bkg_ntuple.root'],    
    'bkg_Zjets'      : [basepath + '/Zjets_bkg_ntuple.root'],    
    'bkg_Znn'        : [basepath + '/Znn_bkg_ntuple.root'],    
}

# CREATING HISTOGRAMS
h = {}
for sample in samples.keys():
    h[sample] = {}

    for hist in hists:
        h[sample][hist] = {}

        n = 'h_%s_%s' % (sample, hist)

        if hist == 'jet1_pt' or hist == 'jet2_pt' or hist == 'LJjet1_pt': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 500000)
        elif hist == 'jet1_eta' or hist == 'jet2_eta' or hist == 'LJjet1_eta': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, -3, 3)
        elif hist == 'jet1_phi' or hist == 'jet2_phi' or hist == 'dphijj' or hist == 'LJjet1_phi': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, -3.4, 3.4)
        elif hist == 'dphi_j1met' or hist == 'min_dphi_jetmet': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 3.4)
        elif hist == 'LJjet1_m': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 300000)
        elif hist == 'jet1_e' or hist == 'jet2_e': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 700000)
        elif hist == 'mjj': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 100000, 6000000)
        elif hist == 'detajj': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 3, 9)
        elif hist == 'signetajj': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, -1.1, 1.1)
        elif hist == 'nLJjets20': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 6, 0, 5)
        elif hist == 'LJjet1_EMfrac': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 1)
        elif hist == 'LJjet1_gapRatio': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 1)
        elif hist == 'LJjet1_IsBIB': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 0.0000001)
        elif hist == 'LJjet1_jvt': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 1)
        elif hist == 'LJjet1_width': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 0.3)
        elif hist == 'LJjet1_timing': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, -10, 10)
        elif hist == 'LJjet1_DPJtagger': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 1)
        elif hist == 'LJjet1_truthDPidx': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 10)
        elif hist == 'MET': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 2000000)
        elif hist == 'METsig': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 80)
        elif hist == 'METOSqrtHT': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 50, 0, 25)
        elif hist == 'njet30': h[sample][hist] = ROOT.TH1F(n, n + "; %s_%s [GeV]" % (hist, sample), 6, 0, 5)
                
# FILLING
for hist in hists:
    print( 'processing histogram: %s' % hist)

    for sample in samples.keys():
        print ('processing sample: %s' % sample)
        l = samples[sample]
        
        for fname in l:
            print ('            > %s' % fname)
            f = ROOT.TFile.Open(fname)
            t = f.Get(tree)
            
            if hist == 'jet1_pt' or hist == 'jet2_pt' or hist == 'LJjet1_pt': n = 'htmp(50,0,500000)'
            elif hist == 'jet1_eta' or hist == 'jet2_eta' or hist == 'LJjet1_eta': n = 'htmp(50,-3,3)'
            elif hist == 'dphi_j1met' or hist == 'min_dphi_jetmet': n = 'htmp(50,0,3.4)'
            elif hist == 'jet1_phi' or hist == 'jet2_phi' or hist == 'dphijj' or hist == 'LJjet1_phi': n = 'htmp(50,-3.4,3.4)'
            elif hist == 'jet1_e' or hist == 'jet2_e': n = 'htmp(50,0,700000)'
            elif hist == 'LJjet1_m': n = 'htmp(50,0,300000)'
            elif hist == 'mjj': n = 'htmp(50,100000,6000000)'
            elif hist == 'detajj': n = 'htmp(50,3,9)'
            elif hist == 'signetajj': n = 'htmp(50,-1.1,1.1)'
            elif hist == 'nLJjets20': n = 'htmp(6,0,5)'
            elif hist == 'LJjet1_width': n = 'htmp(50,0,0.3)'
            elif hist == 'LJjet1_EMfrac': n = 'htmp(50,0,1)'
            elif hist == 'LJjet1_jvt': n = 'htmp(50,0,1)'
            elif hist == 'LJjet1_gapRatio': n = 'htmp(50,0,1)'
            elif hist == 'LJjet1_IsBIB': n = 'htmp(50,0,0.0000001)'
            elif hist == 'LJjet1_timing': n = 'htmp(50,-10,10)'
            elif hist == 'LJjet1_DPJtagger': n = 'htmp(50,0,1)'
            elif hist == 'LJjet1_truthDPidx': n = 'htmp(50,0,10)'
            elif hist == 'MET': n = 'htmp(50,0,2000000)'
            elif hist == 'METsig': n = 'htmp(50,0,80)'
            elif hist == 'METOSqrtHT': n = 'htmp(50,0,25)'
            elif hist == 'njet30': n = 'htmp(6,0,5)'
            
            if t.Draw('%s>>%s' % (hist, n), '1*weight', 'goff'): h[sample][hist].Add(ROOT.gDirectory.Get('htmp'), 1.) # weight for signal is too close to 0
            #if t.Draw('%s>>%s' % (hist, n), '1', 'goff'): h[sample][hist].Add(ROOT.gDirectory.Get('htmp'), 1.) # weight for signal is too close to 0
               
del f

# NORMALISE & CONFIG
for sample in samples.keys():
    for hist in hists:

        if Normalise==True:
            norm = 1;
            if h[sample][hist].Integral() > 0:
                scale = norm/h[sample][hist].Integral()
                h[sample][hist].Scale(scale)
                
        # Style and colour
        h[sample][hist].SetLineWidth(2) # was 1
        h[sample][hist].SetLineStyle(1)
        h[sample][hist].SetFillStyle(3004)
        h[sample][hist].SetLineColor(ROOT.kBlack)
        if ( sample == 'sgn_VBF_500758' ) :
            h[sample][hist].SetFillColor(ROOT.kBlack)
            h[sample][hist].SetLineColor(ROOT.kBlack)
        if ( sample == 'bkg_QCD' ) :
            h[sample][hist].SetFillColor(ROOT.kRed)
            h[sample][hist].SetLineColor(ROOT.kRed+2)
        elif ( sample == 'bkg_top' ) :
            h[sample][hist].SetFillColor(ROOT.kViolet)
            h[sample][hist].SetLineColor(ROOT.kViolet+2)
        elif ( sample == 'bkg_VV' ) :
            h[sample][hist].SetFillColor(ROOT.kGray)
            h[sample][hist].SetLineColor(ROOT.kGray+2)
        elif ( sample == 'bkg_Wjets' ) :
            h[sample][hist].SetFillColor(ROOT.kCyan)
            h[sample][hist].SetLineColor(ROOT.kCyan+2)
        elif ( sample == 'bkg_Zjets' ) :
            h[sample][hist].SetFillColor(ROOT.kGreen)
            h[sample][hist].SetLineColor(ROOT.kGreen+2)
        elif ( sample == 'bkg_Znn' ) :
            h[sample][hist].SetFillColor(ROOT.kYellow)
            h[sample][hist].SetLineColor(ROOT.kYellow+2)

            
# PLOTTING
for hist in hists:
        
    # Tex in canvas - energy
    tex = ROOT.TLatex(0.2, 0.8, "FRVZ model m_{#gamma_{d}} = 0.1 GeV")
    tex.SetName('tex')
    tex.SetNDC(True)
    tex.SetTextSize(0.04)
    # Tex in canvas - luminosity
    texlum = ROOT.TLatex(0.2, 0.75, "#sqrt{s} = 13 TeV, L = 140 fb^{-1}")
    texlum.SetName('tex')
    texlum.SetNDC(True)
    texlum.SetTextSize(0.03)
    texSR = ROOT.TLatex(0.2, 0.71, "SR: Baseline selection")
    texSR.SetName('tex')
    texSR.SetNDC(True)
    texSR.SetTextSize(0.02)
    
    #legend configuration
    if allBkg == True:
        xmin = 0.75
        xmax = 0.9 
        ymin = 0.75 
        ymax = 0.90
    else:
        xmin = 0.75
        xmax = 0.9 
        ymin = 0.62 
        ymax = 0.90
        
    legend = ROOT.TLegend(xmin, ymin, xmax, ymax) 
    legend.SetFillColor(0)
    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.SetTextFont(42)
    legend.SetTextSize(0.03) 
    legend.SetName('Leg')
    legend.SetShadowColor(0)

    legend.AddEntry(h['sgn_VBF_500758'][hist], 'Sgn 0.1 GeV', 'f')
    legend.AddEntry(h['bkg_QCD'][hist], 'QCD', 'f')
    legend.AddEntry(h['bkg_top'][hist], 'Top', 'f')
    legend.AddEntry(h['bkg_VV'][hist], 'VV', 'f')
    legend.AddEntry(h['bkg_Wjets'][hist], 'W+jets', 'f')
    legend.AddEntry(h['bkg_Zjets'][hist], 'Z+jets', 'f')
    legend.AddEntry(h['bkg_Znn'][hist], 'Z+#nu#nu', 'f')
    
    # THStack
    hs  = ROOT.THStack("hsStack","Histogram Comparison")

    hs.Add(h['bkg_QCD'][hist])
    hs.Add(h['bkg_Wjets'][hist])
    hs.Add(h['bkg_Zjets'][hist])
    hs.Add(h['bkg_top'][hist])
    hs.Add(h['bkg_VV'][hist])
    hs.Add(h['sgn_VBF_500758'][hist])
    hs.Add(h['bkg_Znn'][hist])
    
    c = ROOT.TCanvas('c', 'c', 800, 800)
    if doLogY == True: c.SetLogy()
    if doLogX == True: c.SetLogx()    
    hs.Draw('nostack, hist')
    
    # Y-axis title
    hs.GetYaxis().SetLabelSize(0.04)
    if Normalise == True: hs.GetYaxis().SetTitle('Normalise / %.2f' % (h['bkg_QCD'][hist].GetBinWidth(1))) 
    else                : hs.GetYaxis().SetTitle('Events / %.2f' % (h['bkg_QCD'][hist].GetBinWidth(1))) 
    
    # X-axis title
    hs.GetXaxis().SetLabelSize(0.04)
    if hist == 'jet1_pt': hs.GetXaxis().SetTitle('leading jet p_{T} [GeV]')
    elif hist == 'jet1_eta': hs.GetXaxis().SetTitle('leading jet #eta')
    elif hist == 'jet1_phi': hs.GetXaxis().SetTitle('leading jet #phi')
    elif hist == 'jet1_e': hs.GetXaxis().SetTitle('leading jet E [GeV]')
    elif hist == 'jet2_pt': hs.GetXaxis().SetTitle('subleading jet p_{T} [GeV]')
    elif hist == 'jet2_eta': hs.GetXaxis().SetTitle('subleading jet #eta')
    elif hist == 'jet2_phi': hs.GetXaxis().SetTitle('subleading jet #phi')
    elif hist == 'jet2_e': hs.GetXaxis().SetTitle('subleading jet E [GeV]')
    elif hist == 'mjj': hs.GetXaxis().SetTitle('m_{jj} [GeV]')
    elif hist == 'detajj': hs.GetXaxis().SetTitle('#eta(jj)')
    elif hist == 'signetajj': hs.GetXaxis().SetTitle('sign #eta(jj)')
    elif hist == 'dphijj': hs.GetXaxis().SetTitle('#phi(jj)')
    elif hist == 'dphi_j1met': hs.GetXaxis().SetTitle('#phi(leadJet-MET)')
    elif hist == 'min_dphi_jetmet': hs.GetXaxis().SetTitle('min #phi(jet-MET)')
    elif hist == 'LJjet1_pt': hs.GetXaxis().SetTitle('leading lepton-jet p_{T} [GeV]')
    elif hist == 'LJjet1_eta': hs.GetXaxis().SetTitle('leading lepton-jet #eta')
    elif hist == 'LJjet1_phi': hs.GetXaxis().SetTitle('leading lepton-jet #phi')
    elif hist == 'LJjet1_m': hs.GetXaxis().SetTitle('leading lepton-jet m [GeV]')
    elif hist == 'nLJjets20': hs.GetXaxis().SetTitle('lepton-jet multiplicity')    
    elif hist == 'LJjet1_width': hs.GetXaxis().SetTitle('leading lepton-jet width [GeV]')
    elif hist == 'LJjet1_EMfrac': hs.GetXaxis().SetTitle('leading lepton-jet EMfrac')
    elif hist == 'LJjet1_timing': hs.GetXaxis().SetTitle('leading lepton-jet timing')
    elif hist == 'LJjet1_jvt': hs.GetXaxis().SetTitle('leading lepton-jet jvt')
    elif hist == 'LJjet1_gapRatio': hs.GetXaxis().SetTitle('leading lepton-jet gapRatio')
    elif hist == 'LJjet1_IsBIB': hs.GetXaxis().SetTitle('leading lepton-jet isBIB')
    elif hist == 'LJjet1_DPJtagger': hs.GetXaxis().SetTitle('leading lepton-jet DPJtagger')
    elif hist == 'LJjet1_truthDPidx': hs.GetXaxis().SetTitle('leading lepton-jet truthDPidx')
    elif hist == 'MET': hs.GetXaxis().SetTitle('MET [GeV]')
    elif hist == 'METsig': hs.GetXaxis().SetTitle('MET sig')
    elif hist == 'METOSqrtHT': hs.GetXaxis().SetTitle('METOSqrtHt')
    elif hist == 'njet30': hs.GetXaxis().SetTitle('jet multiplicity')
    else: hs.GetXaxis().SetTitle('default')
    
    if Normalise == True:
        hs.SetMaximum(hs.GetMaximum() * 0.6)
    else:
        hs.SetMaximum(hs.GetMaximum() * 10000000)
#        if allBkg == True: hs.SetMaximum(hs.GetMaximum() * 1.0) #was 0.78, 0.68 
#        else: hs.SetMaximum(hs.GetMaximum() * 0.4) #was 0.78, 0.68 
#    else: hs.SetMaximum(pow(10,6)) #was 0.9
    
    legend.Draw()
    tex.Draw()
    texlum.Draw()
    texSR.Draw()
    ATLASLabel(0.18,0.87,"Work in progress")
    c.Update()

    if ( Normalise == True ):
        c.SaveAs( cwd + '/plots/Distributions/Normalise/%s.pdf' % (hist))
        c.SaveAs( cwd + '/plots/Distributions/Normalise/%s.png' % (hist))                
    else:
        c.SaveAs( cwd + '/plots/Distributions/Default/%s.pdf' % (hist))
        c.SaveAs( cwd + '/plots/Distributions/Default/%s.png' % (hist))                
